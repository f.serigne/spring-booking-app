-- Active: 1652171504071@@127.0.0.1@3306@p18_booking

DROP TABLE IF EXISTS type;  

DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS user;

use p18_booking;


CREATE TABLE type (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL    
);

 CREATE TABLE room(
    id INT PRIMARY KEY AUTO_INCREMENT,
    number VARCHAR(128),
    description VARCHAR(256),
    picture VARCHAR(256),
    price DOUBLE NOT NULL,
    id_type INT,
FOREIGN KEY (id_type) references `type`(id) ON DELETE CASCADE
    );
CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    email VARCHAR(256) NOT NULL,
    password VARCHAR(256) NOT NULL,
    address VARCHAR (250),
    role VARCHAR (250) NOT NULL
);
CREATE TABLE booking(
    id INT PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(250),
    date DATE,
  id_room INT,
  id_user INT,
  FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE CASCADE,
  FOREIGN KEY (id_room) references room(id) ON DELETE CASCADE
);
   
    


INSERT INTO type (name) VALUES
("chambre_simple"),
("chambre_double"),
("chambre_familiale"),
("chambre_luxe");


INSERT INTO user (name,email,password,address,role) VALUES
("Serigne","f.serigne@gmail.com",1234,"Lyon","ROLE_ADMIN"),
("Tahir","@gmail.com",1234,"Paris","ROLE_USER"),
("Marc","marc@gmail.com",1234,"Paris","ROLE_AD");

INSERT INTO room (number,picture,description,price, id_type) VALUES
("1A","https://www.deconome.com/wp-content/uploads/2021/04/chambre-2-lits-simples-cote-a-cote.jpg","chambre confortable pour 2 personnes", 90.99, 2),
("2A","https://www.deco.fr/sites/default/files/styles/width_880/public/migration-images/101529.webp?itok=Mh1sTy6h","chambre individuelle économique", 30.50, 1),
("3A","https://img.freepik.com/photos-gratuite/lit-double-dans-appartements-hotel-confortable_185193-81651.jpg?w=826&t=st=1662399238~exp=1662399838~hmac=b725ac8123dc978db0bd363c0850de865d099500be309384a3e81728f32ed130","chambre spacieuse idéale pour famille nombreuse", 125.75, 3),
("4A","https://previews.123rf.com/images/dit26978/dit269781711/dit26978171100011/89973603-suite-de-luxe-moderne-de-chambre-%C3%A0-coucher-de-rendu-3d-dans-l-h%C3%B4tel-avec-le-d%C3%A9cor.jpg","chambre luxueuse confort optimal",299.99, 4),
("4B","https://i.pinimg.com/originals/54/9b/d5/549bd5f59c1c1e58cc1bfdd10cd09330.jpg","chambre bling bling ",259.99,4),
("3B","https://media.istockphoto.com/photos/breakfast-served-on-a-hotel-bed-picture-id936331412","chambre large pouvant accueillir 3 personnes voire plus",120.90,3),
("4C","https://www.designferia.com/sites/default/files/styles/article_images__s640_/public/field/image/1-chambre-design-moderne.jpg?itok=HPjM3_re","chambre de haute gamme confort garanti",350.90,4),
("3C","https://media.istockphoto.com/photos/cozy-and-elegant-bedroom-with-king-size-bed-picture-id1342056639","chambre très grande avec lit grande taille",150.90,3),
("2B","https://media.istockphoto.com/photos/orange-color-pillow-on-white-bed-in-bedroom-picture-id465063546","chambre standard idéal pour court séjour ", 35.50, 1),
("2C","https://img.freepik.com/photos-premium/interieur-chambre-luxe-moderne-dans-studios_97694-13755.jpg?w=826 ","chambre moyenne à prix abordable",39.50, 1),
("1B","https://www.deco.fr/sites/default/files/styles/amp_1200_wide_no_upscale/public/migration-images/101525.jpg?itok=Mah-XOXW","chambre idéal pour couple avec enfant", 100.99, 2),
("1C","https://previews.123rf.com/images/dit26978/dit269781902/dit26978190200034/117814732-chambre-%C3%A0-coucher-moderne-de-luxe-%C3%A0-rendu-3d-avec-t%C3%A9l%C3%A9vision-avec-placard-et-dressing.jpg","chambre pour un jeune couple  ", 110.99, 2);
INSERT INTO booking (description,date, id_room, id_user)VALUES
("text1",'2022-09-05',1,1),
("text2",'2022-08-05',2,1),
("text3",'2022-09-04',1,2),
("text4",'2021-09-05',2,3);


 SELECT * FROM room INNER JOIN type AS t ON id_type = t.id