package co.simplon.promo18.springbookingapp.entities;

public class Room {
   
    private Integer id;
    private String number;
    private String picture;
    private String description;
    private Double price;
    private Type type;
    private Booking booking;
    public Room(Integer id, String number, String picture, String description, Double price) {
        this.id = id;
        this.number = number;
        this.picture = picture;
        this.description = description;
        this.price = price;
    }
    public Room(Integer id, String number, String picture, String description, Double price, Type type) {
        this.id = id;
        this.number = number;
        this.picture = picture;
        this.description = description;
        this.price = price;
        this.type = type;
    }
    public Room() {
    }
    public Room(String number, String picture, String description, Double price, Type type, Booking booking) {
        this.number = number;
        this.picture = picture;
        this.description = description;
        this.price = price;
        this.type = type;
        this.booking = booking;
    }
    public Room(Integer id, String number, String picture, String description, Double price, Type type,
            Booking booking) {
        this.id = id;
        this.number = number;
        this.picture = picture;
        this.description = description;
        this.price = price;
        this.type = type;
        this.booking = booking;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public Booking getBooking() {
        return booking;
    }
    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}