package co.simplon.promo18.springbookingapp.entities;

import java.util.ArrayList;
import java.util.List;

public class Type {
    private int id;
    private String name;
    private List<Room> rooms = new ArrayList<>();

    public Type() {
    }
    public Type(String name) {
        this.name = name;
    }
    public Type(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<Room> getRooms() {
        return rooms;
    }
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }
}
