package co.simplon.promo18.springbookingapp.entities;

import java.util.Collection;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User implements UserDetails {
  
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;
    @NotBlank
    @Size(min=4, max=64)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) //On ignore le password en Json en lecture pour que le mot de passe se balade pas trop
    private String name;
    @Email
    @NotBlank
    private String email;
    private String password;
    private String address;
    private String role;
    public User() {
    }
    public User(@Email @NotBlank @Size(min = 4, max = 64) String name, String email, String password, String address,
            String role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.role = role;
    }
    public User(Integer id, @Email @NotBlank @Size(min = 4, max = 64) String name, String email, String password,
            String address, String role) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.role = role;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(
            new SimpleGrantedAuthority(role)
        );
    }
    @Override
    public String getUsername() {
        return email;
    }
   
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object object) {
        if(object instanceof User) {
            return ((User)object).id.equals(id);
        }
        return false;
    }
    
}
