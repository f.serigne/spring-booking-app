package co.simplon.promo18.springbookingapp.entities;

import java.time.LocalDate;

public class Booking {
    
    private Integer id;
    private String description;
    private LocalDate date;
    private Room room;
    private User user;
   
    public Booking(String description, LocalDate date, Room room, User user) {
        this.description = description;
        this.date = date;
        this.room = room;
        this.user = user;
    }
    public Booking(Integer id, String description, LocalDate date, Room room, User user) {
        this.id = id;
        this.description = description;
        this.date = date;
        this.room = room;
        this.user = user;
    }
    public Booking() {
    }
    public Booking(String description, LocalDate date, Room room) {
        this.description = description;
        this.date = date;
        this.room = room;
    }
    public Booking(Integer id, String description, LocalDate date, Room room) {
        this.id = id;
        this.description = description;
        this.date = date;
        this.room = room;
    }
    public Booking(int int1, String string, LocalDate localDate) {
    }
    public Booking(int int1, int int2, LocalDate localDate) {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public Room getRoom() {
        return room;
    }
    public void setRoom(Room room) {
        this.room = room;
    }
    public String getNumber() {
        return null;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}