package co.simplon.promo18.springbookingapp.auth;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import co.simplon.promo18.springbookingapp.entities.User;
import co.simplon.promo18.springbookingapp.repository.UserRepository;

@Service
public class authservice implements UserDetailsService{
    @Autowired
    private UserRepository Repo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = Repo.findByEmail(email);
        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }   
          return user;  
        }
        
        
    }
    

