package co.simplon.promo18.springbookingapp.auth;

public enum UserRoles {
    ROLE_USER,
    ROLE_ADMIN
}
