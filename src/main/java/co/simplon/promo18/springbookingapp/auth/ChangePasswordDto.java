package co.simplon.promo18.springbookingapp.auth;

public class ChangePasswordDto {
    public String oldPassword;
    public String newPassword;
}
