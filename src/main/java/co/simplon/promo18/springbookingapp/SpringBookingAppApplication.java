package co.simplon.promo18.springbookingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBookingAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBookingAppApplication.class, args);
	}

}
