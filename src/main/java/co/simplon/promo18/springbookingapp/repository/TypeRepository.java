package co.simplon.promo18.springbookingapp.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springbookingapp.entities.Type;

@Repository
public class TypeRepository {

  @Autowired
  private DataSource dataSource;


  public List<Type> findAll() {
    List<Type> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM type");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Type type = new Type(
          rs.getInt("id"),
          rs.getString("name"));

        list.add(type);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public Type findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM type WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Type type = new Type(
          rs.getInt("id"),
          rs.getString("name")); 
          
        return type;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return null;
  } 
  
}