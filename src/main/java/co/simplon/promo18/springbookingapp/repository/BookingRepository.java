package co.simplon.promo18.springbookingapp.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springbookingapp.entities.Booking;
import co.simplon.promo18.springbookingapp.entities.Room;

@Repository
public class BookingRepository {
    @Autowired
    private DataSource dataSource;

    
  public List<Booking> findAll() {
    List<Booking> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM booking LEFT JOIN room AS r ON id_room = r.id");
      
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Booking booking = new Booking(
          rs.getInt("id"),
          rs.getString("description"),
          rs.getDate("date").toLocalDate()
          );  
          Room room = new Room(
            rs.getInt("r.id"),
            rs.getString("r.number"),
            rs.getString("r.picture"),
            rs.getString("r.description"), 
            rs.getDouble("r.price"));
          booking.setRoom(room);

        list.add(booking);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public Booking findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM booking LEFT JOIN room AS r ON id_room = r.id WHERE booking.id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Booking booking = new Booking(
         rs.getInt("id"),
         rs.getString("descrition"),
         rs.getDate("date").toLocalDate()
         );
         Room room = new Room(
          rs.getInt("r.id"),
          rs.getString("r.number"),
          rs.getString("r.picture"),
          rs.getString("r.description"), 
          rs.getDouble("r.price"));
          booking.setRoom(room); 
          
        return booking;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return null;
  }

  public void save(Booking booking) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO booking (description, date, id_room, id_user) VALUES (?,?,?,?)",
              Statement.RETURN_GENERATED_KEYS);

      
      stmt.setString(1, booking.getDescription());
      stmt.setDate(2, Date.valueOf(booking.getDate()));
      stmt.setInt(3, booking.getRoom().getId());
      stmt.setInt(4, booking.getUser().getId());
      

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        booking.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

}