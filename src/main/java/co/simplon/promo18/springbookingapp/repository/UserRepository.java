package co.simplon.promo18.springbookingapp.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springbookingapp.entities.User;

@Repository
public class UserRepository{
  
    @Autowired
    private DataSource dataSource;

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToUser(rs));
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return list;
    }


    public User findByEmail(String email) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email =?");

            stmt.setString(1, email);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToUser(rs);
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return null;
    }

    public List<User> findByName(String name) {
        List<User> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE name=?");

            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToUser(rs));
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return list;
    }


    
    public void save(User user) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO user (name, email, password,address, role) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getName());
            stmt.setString(2, user.getEmail());
            stmt.setString(3, user.getPassword());
            stmt.setString(4, user.getAddress());
            stmt.setString(5, user.getRole());
          
            

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM user WHERE id=?");
            stmt.setInt(1, id);

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error",e);
        }
    }

    public boolean update(User user) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE user SET name = ?, email = ?, password = ?, address = ?, role = ? WHERE id = ?");
           stmt.setString(1, user.getName());
           stmt.setString(2, user.getEmail());
           stmt.setString(3, user.getPassword());
           stmt.setString(4, user.getAddress());
           stmt.setString(5, user.getRole());
           stmt.setInt(6, user.getId());

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

    }


    public static User sqlToUser(ResultSet rs) throws SQLException {
        return new User(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getString("email"),
            rs.getString("password"),
            rs.getString("address"),
            rs.getString("role"));
    }

    

}
