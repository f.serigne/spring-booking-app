package co.simplon.promo18.springbookingapp.repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.springbookingapp.repository.RoomRepository;
import co.simplon.promo18.springbookingapp.entities.Room;
import co.simplon.promo18.springbookingapp.entities.Type;


@Repository
public class RoomRepository {

  @Autowired
  private DataSource dataSource;


  public List<Room> findAll() {
    List<Room> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM room r INNER JOIN type t ON id_type = t.id");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Room room = new Room(
          rs.getInt("r.id"),
          rs.getString("r.number"),
          rs.getString("r.picture"),
          rs.getString("r.description"),
          rs.getDouble("r.price"),
          new Type(
            rs.getInt("t.id"),
            rs.getString("t.name")
        )
          );
        list.add(room);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public Room findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM room INNER JOIN type ON room.id_type = type.id WHERE room.id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Room room = new Room(
          rs.getInt("room.id"),
          rs.getString("number"),
          rs.getString("picture"),
          rs.getString("description"), 
          rs.getDouble("price"),
          new Type(
            rs.getInt("type.id"),
            rs.getString("name")
          ));


        return room;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return null;
  }

  public void save(Room room) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO room (number, picture,description, price, id_type) VALUES (?,?,?,?,?)",
              Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, room.getNumber());
      stmt.setString(2,room.getPicture());
      stmt.setString(3, room.getDescription());
      stmt.setDouble(4, room.getPrice());
      stmt.setInt(5, room.getType().getId());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        room.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM room WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Room room) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE room SET number = ?,picture=?,description = ?, price = ?,  WHERE id = ?");

      stmt.setString(1, room.getNumber());
      stmt.setString(2, room.getPicture());
      stmt.setString(3, room.getDescription());
      stmt.setDouble(4, room.getPrice());
      stmt.setInt(5, room.getId());

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

  public List<Room> searchByNumber(String number) {
    List<Room> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(

          "SELECT * FROM room WHERE number LIKE ?");
      stmt.setString(1, "%" + number + "%");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Room room = new Room(
          rs.getInt("id"),
          rs.getString("number"),
          rs.getString("picture"),
          rs.getString("description"),
          rs.getDouble("price"));
          

        list.add(room);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public List<Room> findByIdType(int idtype) {
    List<Room> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM room WHERE id_type=?");

        stmt.setInt(1, idtype);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
          Room room = new Room(
            rs.getInt("id"),
            rs.getString("number"),
            rs.getString("picture"),
            rs.getString("description"),
            rs.getDouble("price"));
  
          list.add(room);
        }
      } catch (SQLException e) {
  
        e.printStackTrace();
        throw new RuntimeException("Database access error");
      }
  
      return list;
  }
}
