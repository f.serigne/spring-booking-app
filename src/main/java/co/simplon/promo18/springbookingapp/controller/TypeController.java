package co.simplon.promo18.springbookingapp.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springbookingapp.entities.Type;
import co.simplon.promo18.springbookingapp.repository.TypeRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/type")
public class TypeController {

  @Autowired
  private TypeRepository tr;

  @GetMapping
  public List<Type> all() {
    return tr.findAll();
  }

  @GetMapping("/{id}")
  public Type one(@PathVariable int id) {
    Type type = tr.findById(id);
    if (type == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return type;
  }


}