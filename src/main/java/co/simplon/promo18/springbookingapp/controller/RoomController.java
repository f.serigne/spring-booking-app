package co.simplon.promo18.springbookingapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springbookingapp.entities.Room;
import co.simplon.promo18.springbookingapp.repository.RoomRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/room")
public class RoomController {

  @Autowired
  private RoomRepository repo;


  @GetMapping
  public List<Room> getAll() {
      return repo.findAll();
    }

  @GetMapping("/search/{field}")
  public List<Room> getLike(@PathVariable String field) {
      return repo.searchByNumber(field);
    }
  

  @GetMapping("/{id}")
  public Room one(@PathVariable int id) {
    Room room = repo.findById(id);
    if (room == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return room;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Room addRoom(@RequestBody Room room) {
    room.setId(null);

    repo.save(room);

    return room;
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!repo.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/{type}")
  public Room update(@RequestBody Room room, @PathVariable int id) {
    if (id != room.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!repo.update(room)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return repo.findById(room.getId());
  }

  @PatchMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public Room patch(@RequestBody Room room, @PathVariable int id) {
      Room baseRo = repo.findById(id);
    if (baseRo == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    if (room.getNumber() != null)
       baseRo.setNumber(room.getNumber());
       if (room.getPicture() != null)
       baseRo.setPicture(room.getPicture());   
    if (room.getDescription() != null)
      baseRo.setDescription(room.getDescription());
    if (room.getPrice() != 0)
      baseRo.setPrice(room.getPrice());
    if (!repo.update((Room) baseRo))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return baseRo;
  }
  
  @GetMapping("/type/{id}")
  public List<Room> getRoomsByType(@PathVariable int id){
    List<Room> rooms=repo.findByIdType(id);
    return rooms;
  }

}