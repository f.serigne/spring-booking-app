package co.simplon.promo18.springbookingapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springbookingapp.auth.ChangePasswordDto;
import co.simplon.promo18.springbookingapp.entities.User;
import co.simplon.promo18.springbookingapp.repository.UserRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/account")
    public User getAccount(@AuthenticationPrincipal User user) {
        return user;
    }

    @PostMapping
    public User register(@Valid @RequestBody User user) {
        if (userRepo.findByEmail(user.getEmail()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        user.setId(null);
        user.setRole("ROLE_USER"); // On définit le rôle par défaut à user, pour pas qu'on puisse choisir son rôle
                                   // en s'inscrivant
        // On hash le mot de passe en utilisant notre librairie de hash définie dans la
        // configuration (ici, bcrypt)
        String hashed = encoder.encode(user.getPassword());
        // Et c'est ce hash qu'on stock en base de données, et pas le mot de passe en
        // clair
        user.setPassword(hashed);
        userRepo.save(user);

        // Optionnel, mais avec ça, on peut connecter le User automatiquement lors de
        // son inscription
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));

        return user;
    }

    @PatchMapping("/password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody ChangePasswordDto body, @AuthenticationPrincipal User user) {
        if (!encoder.matches(body.oldPassword, user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Old Password doesn't match");
        }

        String hashed = encoder.encode(body.newPassword);
        user.setPassword(hashed);
        userRepo.save(user);
    }

    @GetMapping("/{email}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public User userExists(@PathVariable String email) {
        User user = userRepo.findByEmail(email);
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return user;

    }

}
