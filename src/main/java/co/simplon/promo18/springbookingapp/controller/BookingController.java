package co.simplon.promo18.springbookingapp.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springbookingapp.entities.Booking;
import co.simplon.promo18.springbookingapp.entities.User;
import co.simplon.promo18.springbookingapp.repository.BookingRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/booking")
@Validated
public class BookingController {

    @Autowired
    private BookingRepository bo;

    @GetMapping
    public List<Booking> all() {
            return bo.findAll();

        }


  @GetMapping("/{id}")
  public Booking one(@PathVariable int id) {
    Booking booking = bo.findById(id);
    if (booking == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return booking;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Booking addBooking(@RequestBody Booking booking, @AuthenticationPrincipal User user) {
    booking.setUser(user);
    bo.save(booking);

    return booking;
  

  
}

}